package com.elib.setting;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "app_setting")
public class Setting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String 	organizationName;
    private String 	address;
    private String 	phone;
    private String 	email;
    private String 	manager;

    public Setting() {
    }
}
