package com.elib.book;

import com.elib.borrow.Borrow;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long bookId;
    private String name;
    private String author;
    private String publishDate;
    private String pageCount;
    private String publisher;

    @OneToMany(mappedBy = "book")
    private List<Borrow> borrows;
}
