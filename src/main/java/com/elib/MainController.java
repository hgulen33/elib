package com.elib;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import javafx.fxml.Initializable;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class MainController implements Initializable {


    private LoginManager loginManager;

    public void initMain(LoginManager loginManager, String token) {
        this.loginManager = loginManager;
        try {

            Claims claims = Jwts.parser().setSigningKey(LoginManager.KEY).parseClaimsJws(token).getBody();

            //OK, we can trust this JWT

        } catch (SignatureException e) {

            //don't trust the JWT!
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
