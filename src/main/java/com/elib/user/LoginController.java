package com.elib.user;

import com.elib.LoginManager;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.soap.SOAPBinding;
import java.net.URL;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;


@Component
public class LoginController implements Initializable {

    private LoginRepository loginRepository;
    private ValidationSupport validationSupport;// = new ValidationSupport();
    private LoginManager loginManager;

    @Autowired
    public LoginController(LoginRepository loginRepository){
        this.loginRepository = loginRepository;
    }

    @FXML
    private PasswordField txtPassword;

    @FXML
    private TextField txtUsername;

    public void Login() {
        if (!validationSupport.isInvalid()) {
            String password = txtPassword.getText();
            String username = txtUsername.getText();
            Optional<User> res = loginRepository.findByUserNameAndPassword(username, password);
            if (res.isPresent()) {
                User usr= res.get();
                Map<String, Object> claims = new HashMap<>();
                claims.put(Claims.ROLE_CLAIM, usr.getRole());
                claims.put(Claims.EMAIL_CLAIM, usr.getEmail());
                claims.put(Claims.USERNAME_CLAIM, usr.getUserName());
                String compactJws = Jwts.builder()
                        .setSubject(usr.getFullName())
                        .setClaims(claims)
                        .signWith(SignatureAlgorithm.HS512, LoginManager.KEY)
                        .compact();
                loginManager.authenticated(compactJws);
            }
            else {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("Kullanıcı adı veya şifre hatalı!");
                a.setTitle("");
                a.setHeaderText("Hata!");
                a.showAndWait();
            }
        }
    }

    private void setValidations() {
        this.validationSupport = new ValidationSupport();
        validationSupport.registerValidator(txtPassword, new PasswordValidator());
        validationSupport.registerValidator(txtUsername, Validator.createEmptyValidator("Kullanıcı adı boş geçilemez."));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setValidations();

    }

    public void initManager(LoginManager loginManager) {

        this.loginManager = loginManager;
    }
}


//public class TimeHHMMValidator implements Validator<String> {
//
//
//    private static final String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
//
//    @Override
//    public ValidationResult apply(Control control, String s) {
//        boolean matches = ((TextField) control).getText().matches(TIME24HOURS_PATTERN);
//        if (!matches)
//            return ValidationResult.fromErrorIf(control, "Hatalı saat biçimi.", !matches);
//        return null;
//    }
//}


class PasswordValidator implements Validator<String> {


//    private static final String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";

    @Override
    public ValidationResult apply(Control control, String s) {
        boolean matches = ((TextField) control).getText().length() > 4;
        if (!matches)
            return ValidationResult.fromErrorIf(control, "Şifre en az 4 karakterden oluşmalı", !matches);
        return null;
    }
}
