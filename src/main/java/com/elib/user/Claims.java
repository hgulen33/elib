package com.elib.user;

public  class Claims {
	public static final String ROLE_CLAIM = "role";
	public static final String EMAIL_CLAIM = "email";
	public static final String USERNAME_CLAIM = "username";
}
