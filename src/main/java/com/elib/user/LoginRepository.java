package com.elib.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.NamedQuery;
import java.util.Optional;

@Repository
public interface LoginRepository extends CrudRepository<User, Long> {

    @Query(value = "select (count(u) > 0) from User u where u.userName=:username and u.password=:password")
    boolean checkUserAndPassword(@Param("username") String username,@Param("password") String password);

    Optional<User> findByUserNameAndPassword(String username, String password);
}