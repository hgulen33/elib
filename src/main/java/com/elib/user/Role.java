package com.elib.user;

public enum Role {
    LIBRARIAN,
    TEACHER,
    STUDENT,
    ADMIN;
}
