package com.elib;

import com.elib.user.LoginController;
import javafx.event.WeakEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

/** Manages control flow for logins */

public class LoginManager {
	private Stage primaryStage;
	private Stage loginStage;
	private ConfigurableApplicationContext configurableApplicationContext;
	private Parent root;
	public static final String KEY  = "Orewanarukaizokuoni";


	public LoginManager(Stage stage, ConfigurableApplicationContext configurableApplicationContext) {
		this.primaryStage = stage;
		this.configurableApplicationContext = configurableApplicationContext;
	}

	/**
	 * Callback method invoked to notify that a user has been authenticated.
	 * Will show the main application screen.
	 */
	public void authenticated(String token) {
		showMainView(token);
	}

	/**
	 * Callback method invoked to notify that a user has logged out of the main application.
	 * Will show the login application screen.
	 */
	public void logout() {
		showLoginScreen();
	}

	public void showLoginScreen() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/user/LoginView.fxml"));
			fxmlLoader.setControllerFactory(configurableApplicationContext::getBean);
			root = fxmlLoader.load();
			loginStage = new Stage();
			loginStage.setTitle("E-Kütüphane yazılımı");
			loginStage.resizableProperty().setValue(false);
			loginStage.initStyle(StageStyle.UTILITY);
			Scene scene = new Scene(root, 300, 120);
			loginStage.setScene(scene);
			loginStage.show();
			LoginController controller = fxmlLoader.getController();
			controller.initManager(this);
		} catch (IOException ex) {
			Logger.getLogger(LoginManager.class.getName()).log(Level.ERROR, null,ex);
		}
	}

	private void showMainView(String token) {
		try {
			loginStage.close();
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/MainView.fxml"));
			fxmlLoader.setControllerFactory(configurableApplicationContext::getBean);
			root = fxmlLoader.load();
			primaryStage.setTitle("KCPAL E-Kütüphane yazılımı v1.0");
//			primaryStage.resizableProperty().setValue(true);
//			primaryStage.initStyle(StageStyle.DECORATED);
			Scene scene = new Scene(root, 900, 600);
			primaryStage.minHeightProperty().setValue(400);
			primaryStage.minWidthProperty().setValue(600);
			primaryStage.setScene(scene);
			primaryStage.show();
			//primaryStage.onCloseRequestProperty().bind();
			MainController controller =
					fxmlLoader.getController();
			controller.initMain(this, token);
		} catch (IOException ex) {
			Logger.getLogger(LoginManager.class.getName()).log(Level.ERROR, null,ex);

		}
	}
}

