package com.elib.borrow;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BorrowRepository extends PagingAndSortingRepository<Borrow, Long> {

}
