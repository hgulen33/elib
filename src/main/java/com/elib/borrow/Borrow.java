package com.elib.borrow;

import com.elib.book.Book;
import com.elib.borrower.Borrower;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Data
@Entity
@Table
public class Borrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long borrowId;

    @ManyToOne
    private Borrower borrower;

    @ManyToOne
    private Book book;

    private LocalDate startDate;

    private LocalDate endDate;

    public int getBorrowDays(){
        return Period.between(endDate, startDate).getDays();
    }
}
