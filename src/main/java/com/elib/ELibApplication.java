package com.elib;

import com.elib.user.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import sun.tools.jar.CommandLine;

import java.io.IOException;
import java.util.Optional;


@Lazy
@EnableJpaRepositories
@EnableConfigurationProperties
@EnableAutoConfiguration
@SuppressWarnings("restriction")
@SpringBootApplication
public class ELibApplication extends AbstractJavaFxApplicationSupport implements CommandLineRunner {
	@Autowired
	UserRepository userRepository;

	@Autowired
	LoginRepository loginRepository;

	@Override
	public void init() throws Exception {
		super.init();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		LoginManager loginManager = new LoginManager(primaryStage, getApplicationContext());
		loginManager.showLoginScreen();
	}


	public static void main(String[] args) {
		launchApp(ELibApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		User admin = new User();
		admin.setFirstName("Admin");
		admin.setLastName("Library");
		admin.setEmail("admin@admin.com.tr");
		admin.setPassword("admin");
		admin.setUserName("admin");
		admin.setRole(Role.ADMIN);
		Optional<User> userOptional = loginRepository.findByUserNameAndPassword(admin.getUserName(), admin.getPassword());
		if (!userOptional.isPresent()) {
			userRepository.save(admin);
		}
	}
}



