package com.elib.borrower;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class BorrowerListController implements Initializable {

    @FXML
    private TableView tblBorrowList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
