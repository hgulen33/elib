package com.elib.borrower;

public enum BorrowerType {
    STUDENT,
    TEACHER,
    MANAGER
}
