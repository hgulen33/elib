package com.elib.borrower;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface BorrowerRepository  extends PagingAndSortingRepository<Borrower, Long> {
}
