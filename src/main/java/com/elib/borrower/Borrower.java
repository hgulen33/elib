package com.elib.borrower;

import com.elib.borrow.Borrow;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table
public class Borrower {

    public Borrower() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long borrowerId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String TCIdentity;
    private BorrowerState borrowerState;

    @OneToMany(mappedBy = "borrower")
    private List<Borrow> borrows;
}

