package com.elib.borrower;

public enum BorrowerState {
    ACTIVE,
    PASSIVE
}
